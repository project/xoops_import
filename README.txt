
Installation
============

Install the followin optional modules:
- Comment
- Forum
- Statistics
- Upload
- Profile

Define the following user profile fields:
  profile_name
  profile_url
  profile_icq
  profile_location
  profile_aim
  profile_yim
  profile_msn
  profile_occupation
  profile_extra (textarea)
  profile_interest
  profile_mailok (checkbox)

Create a new input format for rendered Xoops content. This format should accept full
HTML and it should not enable the line break converter. A format with no filters
enabled is the best. Make sure this format is selected on the Xoops Import
configuration page.

PHP Configuration
=================

In your php.ini file look for the "Resource Limits" section and increase the
following values:
- max_execution_time
- memory_limit
