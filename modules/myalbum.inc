<?php

/*
 Xoops tables: xoops_myalbum_(cat|comments|photos|text|votedata)
 Xoops folders: uploads/photos/ - image name is its id + extension
*/

function xoops_import_module_myalbum_info() {
  $info = array(
    '#name' => t('MyAlbum'),
    '#help' => t('Import Xoops MyAlbum photos as Drupal image galleries'),
  );
  
  return $info;
}

function xoops_import_module_myalbum_configure() {
  $form = array();

  $form['myalbum']['xoops_import_module_myalbum_dirname'] = array(
    '#type' => 'textfield',
    '#title' => t('dirname'),
    '#return_value' => 1,
    '#default_value' => variable_get('xoops_import_module_myalbum_dirname', 'myalbum'),
    '#description' => t('The folder name of the MyAlbum module.'),
  );

  $form['myalbum']['xoops_import_module_myalbum_basetablename'] = array(
    '#type' => 'textfield',
    '#title' => t('base table name'),
    '#field_prefix' => 'xoops_',
    '#field_suffix' => '_(cat|photos|text|votedata)',
    '#size' => 10,
    '#return_value' => 1,
    '#default_value' => variable_get('xoops_import_module_myalbum_basetablename', 'myalbum'),
    '#description' => t('The base table name of the MyAlbum module, this would be different from the default only if the module was cloned and installed several times: myalbum, myalbum0, myalbum1, myalbum2'),
  );
  
  return $form;
}

function xoops_import_module_myalbum_confirm() {
  $myalbum_base_table_name = variable_get('xoops_import_module_myalbum_basetablename', 'myalbum');
  $x_myalbum_module_dir = variable_get('xoops_import_module_mylabum_dirname', 'myalbum');
  
  $disable = FALSE;
  $catCnt = 0;
  $photoCnt = 0;
  
  _xoops_import_db_connect();
  
  $xoops_import_table_preffix = variable_get('xoops_import_table_preffix', 'xoops');

  db_set_active('xoops');
  
  $myAlbumRes = db_query("select COUNT(*) from {$xoops_import_table_preffix}_{$myalbum_base_table_name}_cat");
  
  if ($myAlbumRes) {
    $catCnt = db_result($myAlbumRes);
  } else {
    $disable = TRUE;
  }
  
  $myAlbumRes = db_query("select COUNT(*) from {$xoops_import_table_preffix}_{$myalbum_base_table_name}_photos");
  
  if ($myAlbumRes) {
    $photoCnt = db_result($myAlbumRes);
  } else {
    $disable = TRUE;
  }
  
  db_set_active('default');
  
  $form = array();
  
  if (!_xoops_import_module_was_imported('users')) {
    $disable = TRUE;
    
    $form['users'] = array(
      '#value' => '<p>' . t("Import the users first!") . '</p>',
    );
  }
  
  if (_xoops_import_module_was_imported($x_myalbum_module_dir)) {
    $disable = TRUE;
    
    $form['myalbum'] = array(
      '#value' => '<p>' . t("MyAlbum already imported!") . '</p>',
    );
  }
  
  if (variable_get('xoops_import_base_folder', '') == '') {
    $disable = TRUE;
    
    $form['folder'] = array(
      '#value' => '<p>' . t("Xoops base folder not set!") . '</p>',
    );
  }
  
  $form['cat_cnt'] = array(
    '#value' => '<p>' . t("MyAlbum categories: %cats", array('%cats' => $catCnt)) . '</p>',
  );
  
  $form['photo_cnt'] = array(
    '#value' => '<p>' . t("MyAlbum photos: %photos", array('%photos' => $photoCnt)) . '</p>',
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
    '#disabled' => $disable,
  );
  
  return $form;
}

function xoops_import_module_myalbum_import() {
  $myalbum_base_table_name = variable_get('xoops_import_module_myalbum_basetablename', 'myalbum');
  
  $x_myalbum_module_dir = variable_get('xoops_import_module_mylabum_dirname', 'myalbum');
  $x_myalbum_module_id  = _xoops_import_get_xoops_module_id($x_myalbum_module_dir);
  
  _xoops_import_db_connect();
  
  $xoops_import_table_preffix = variable_get('xoops_import_table_preffix', 'xoops');
  
  $output = '';
  
  $cat_map = _xoops_import_category(
    $output,
    variable_get('image_gallery_nav_vocabulary', ''),
    'MyAlbum Category',
    'MyAlbum Category created by Xoops Import',
    'image',
    $myalbum_base_table_name . '_cat',
    'cid',
    'title',
    '',
    'pid',
    $x_myalbum_module_dir
  );
  
  db_set_active('xoops');

  $xoopsPhotos = db_query("select * from {$xoops_import_table_preffix}_{$myalbum_base_table_name}_photos AS p, {$xoops_import_table_preffix}_{$myalbum_base_table_name}_text AS t WHERE p.lid=t.lid ORDER BY p.lid");
  
  db_set_active('default');

  $base_folder = variable_get('xoops_import_base_folder', '') . '/uploads/photos/';
  
  while ($row = db_fetch_array($xoopsPhotos)) {
    $filepath = $base_folder . $row['lid'] . '.' . $row['ext'];
    
    $output .= "Importing photo {$row['title']}<br/>";
    
    $node = _xoops_import_create_image(
      $filepath,
      $row['title'],
      $row['description'],
      $cat_map[$row['cid']],
      $row['submitter'],
      $row['date'],
      $row['hits']);
    
    if ($node) {
      $nid = $node->nid;
      
      $output .= _xoops_import_comments($x_myalbum_module_dir, $x_myalbum_module_id, $row['lid'], 0, $nid, 0, '');
      
      _comment_update_node_statistics($nid);
      
      _xoops_import_insert_redirect($row['lid'], $nid, $x_myalbum_module_dir);
    } else {
    }
  }
  
  _xoops_import_module_set_imported($x_myalbum_module_dir);
  
  print theme('page', $output);
}

?>
