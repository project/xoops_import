<?php

function xoops_import_module_users_info() {
  $info = array(
    '#name' => t('System Users'),
    '#help' => t('Import Xoops system users as Drupal users'),
  );
  
  return $info;
}

function xoops_import_module_users_configure() {
  $form = array();
  
  return $form;
}

function xoops_import_module_users_confirm() {
  $disable = FALSE;
  $userCnt = 0;
  
  _xoops_import_db_connect();
  
  $xoops_import_table_preffix = variable_get('xoops_import_table_preffix', 'xoops');

  db_set_active('xoops');
  
  $userRes = db_query("SELECT COUNT(*) FROM {$xoops_import_table_preffix}_users WHERE uid>1 AND last_login>0");
  
  if ($userRes) {
    $userCnt = db_result($userRes);
  } else {
    $disable = TRUE;
  }
  
  db_set_active('default');
  
  $form = array();
  
  if (_xoops_import_module_was_imported('users')) {
    $disable = TRUE;
    
    $form['users'] = array(
      '#value' => '<p>' . t("Users imported already!") . '</p>',
    );
  } else {
    $userRes = db_query("SELECT MAX(uid) FROM {users}");
    $maxUid = db_result($userRes);
    
    if ($maxUid != 1) {
      $disable = TRUE;
      
      $form['users'] = array(
        '#value' => '<p>' . t("You already created users in the Drupal site! You cannot import from Xoops.") . '</p>',
      );
    }
  }
  
  $form['user_cnt'] = array(
    '#value' => '<p>' . t("Xoops user count: %count", array('%count' => $userCnt)) . '</p>',
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
    '#disabled' => $disable,
  );
  
  return $form;
}

function xoops_import_module_users_import() {
  $profile_field_map = array();
  
  $profileRes = db_query('select fid, name from {profile_fields}');
  
  while ($row = db_fetch_array($profileRes)) {
    $profile_field_map[$row['name']] = $row['fid'];
  }
  
  _xoops_import_db_connect();
  
  $xoops_import_table_preffix = variable_get('xoops_import_table_preffix', 'xoops');

  db_set_active('xoops');
  
  $xoopsUsers = db_query("select * from {$xoops_import_table_preffix}_users WHERE uid>1 AND last_login>0 ORDER BY uid");
  
  db_set_active('default');
  
  $output = '';
  $lastId = 0;
  
  while ($row = db_fetch_array($xoopsUsers)) {
    
    $uid = intval($row['uid']);
    
    db_query('INSERT INTO {users} (uid,name,pass,mail,signature,created,access,login,status,timezone,init,data) VALUES (%d,\'%s\',\'%s\',\'%s\',\'%s\',%d,%d,%d,%d,%d,\'%s\',\'%s\')',
      $uid,    // uid
      $row['uname'],            // name
      $row['pass'],           // pass
      $row['email'],            // mail
      $row['user_sig'],            // signature TODO: convert format!
      intval($row['user_regdate']),    // created
      intval($row['last_login']),   // access
      intval($row['last_login']),   // login
      1,                                      // status
      intval($row['timezone_offset'] * 60 * 60),             // timezone
      $row['email'],                                // init
      'a:0:{}'                                // data
    );
    
    db_query('INSERT INTO {users_roles} (uid,rid) VALUES (%d,%d)', $uid, 2);
    
    _xoops_import_users_profile_field($uid, $profile_field_map, 'profile_name', $row['name']);
    _xoops_import_users_profile_field($uid, $profile_field_map, 'profile_url', $row['url']);
    _xoops_import_users_profile_field($uid, $profile_field_map, 'profile_icq', $row['user_icq']);
    _xoops_import_users_profile_field($uid, $profile_field_map, 'profile_location', $row['user_from']);
    _xoops_import_users_profile_field($uid, $profile_field_map, 'profile_aim', $row['user_aim']);
    _xoops_import_users_profile_field($uid, $profile_field_map, 'profile_yim', $row['user_yim']);
    _xoops_import_users_profile_field($uid, $profile_field_map, 'profile_msn', $row['user_msnm']);
    _xoops_import_users_profile_field($uid, $profile_field_map, 'profile_occupation', $row['user_occ']);
    _xoops_import_users_profile_field($uid, $profile_field_map, 'profile_extra', $row['bio']);
    _xoops_import_users_profile_field($uid, $profile_field_map, 'profile_interest', $row['user_intrest']);
    _xoops_import_users_profile_field($uid, $profile_field_map, 'profile_mailok', $row['user_mailok']);
    
    $lastId = $uid;
    
    $output .= "Importing user {$row['uname']} - $lastId<br>";		
  }

  $output .= "Updating last id: " . (intval($lastId)+1) . "<br>";
  
  db_query('update sequences set id = %d where name = \'users_uid\'', intval($lastId)+1);
  
  _xoops_import_module_set_imported('users');
  
  print theme('page', $output);
  
  return;
}

function _xoops_import_users_profile_field($uid, $field_map, $field_name, $value) {
  if (!empty($value)) {
    if (isset($field_map[$field_name])) {
      $field_id = $field_map[$field_name];
      
      db_query('INSERT INTO {profile_values} (fid, uid, value) VALUES (%d,%d,\'%s\')',
        $field_id,
        $uid,
        $value
      );
    }
  }
}

?>
