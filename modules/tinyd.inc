<?php

function xoops_import_module_tinyd_info() {
  $info = array(
    '#name' => t('TinyD'),
    '#help' => t('Import Xoops TinyD pages as Drupal nodes'),
  );
  
  return $info;
}

function xoops_import_module_tinyd_configure() {
  $form = array();

  $form['tinyd']['xoops_import_module_tinyd_dirname'] = array(
    '#type' => 'textfield',
    '#title' => t('dirname'),
    '#default_value' => variable_get('xoops_import_module_tinyd_dirname', 'tinyd0'),
    '#description' => t('The folder name of the TinyD module.'),
  );

  $form['tinyd']['xoops_import_module_tinyd_basetablename'] = array(
    '#type' => 'textfield',
    '#title' => t('base table name'),
    '#field_prefix' => 'xoops_',
    '#size' => 10,
    '#default_value' => variable_get('xoops_import_module_tinyd_basetablename', 'tinycontent0'),
    '#description' => t('The base table name of the TinyD module, this would be different from the default only if the module was cloned using search and replace and installed several times.'),
  );
  
  $form['tinyd']['xoops_import_module_tinyd_nodetype'] = array(
    '#type' => 'select',
    '#title' => t('Content Type'),
    '#options' => node_get_types('names'),
    '#default_value' => variable_get('xoops_import_module_tinyd_nodetype', 'page'),
    '#description' => t('The content type to import TinyD pages into.'),
  );

  return $form;
}

function xoops_import_module_tinyd_confirm() {
  $tinyd_base_table_name = variable_get('xoops_import_module_tinyd_basetablename', 'tinycontent0');
  $x_tinyd_module_dir = variable_get('xoops_import_module_tinyd_dirname', 'tinyd0');
  
  $disable = FALSE;
  $pageCnt = 0;
  
  _xoops_import_db_connect();
  
  $xoops_import_table_preffix = variable_get('xoops_import_table_preffix', 'xoops');

  db_set_active('xoops');
  
  $tinydRes = db_query("select COUNT(*) from {$xoops_import_table_preffix}_{$tinyd_base_table_name}");
  
  if ($tinydRes) {
    $pageCnt = db_result($tinydRes);
  } else {
    $disable = TRUE;
  }
  
  db_set_active('default');
  
  $form = array();
  
  if (_xoops_import_module_was_imported($x_tinyd_module_dir)) {
    $disable = TRUE;
    
    $form['tinyd'] = array(
      '#value' => '<p>' . t("TinyD already imported!") . '</p>',
    );
  }
  
  if (variable_get('xoops_import_module_tinyd_nodetype', '') == '') {
    $disable = TRUE;
    
    $form['nodetype'] = array(
      '#value' => '<p>' . t("Select a content type to import into!") . '</p>',
    );
  }
  
  $form['page_cnt'] = array(
    '#value' => '<p>' . t("TinyD pages: %pages", array('%pages' => $pageCnt)) . '</p>',
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
    '#disabled' => $disable,
  );
  
  return $form;
}

function xoops_import_module_tinyd_import() {
  $tinyd_base_table_name = variable_get('xoops_import_module_tinyd_basetablename', 'tinycontent0');

  _xoops_import_db_connect();
  
  $xoops_import_table_preffix = variable_get('xoops_import_table_preffix', 'xoops');
  
  $output = '';
  
  db_set_active('xoops');

  $xoopsPages = db_query("select * from {$xoops_import_table_preffix}_{$tinyd_base_table_name} ORDER BY storyid");
  
  db_set_active('default');
  
  $x_tinyd_module_dir = variable_get('xoops_import_module_tinyd_dirname', 'tinyd0');
  $x_tinyd_module_id  = _xoops_import_get_xoops_module_id($x_tinyd_module_dir);
  
  $fields = node_invoke_nodeapi($node, 'fields');
  
  while ($row = db_fetch_array($xoopsPages)) {
    
    $nid = db_next_id('{node}_nid');
    $vid = db_next_id('{node_revisions}_vid');
    
    _xoops_import_insert_redirect($row['storyid'], $nid, $x_tinyd_module_dir);
    
    $output .= "Importing page {$row['title']}<br/>";
  
    db_query('INSERT INTO {node} (nid,vid,type,title,uid,status,created,changed,comment,promote) VALUES(%d,%d,\'%s\',\'%s\',%d,%d,%d,%d,%d,%d)',
      $nid,
      $vid,
      variable_get('xoops_import_module_tinyd_nodetype', ''),
      $row['title'],
      1,
      $row['visible'],
      $row['created'],
      $row['last_modified'],
      $row['nocomments'] == 1 ? 1 : 2,
      0
    );
    db_query('INSERT INTO {node_revisions} (nid,vid,uid,title,body,teaser,timestamp,format) VALUES(%d,%d,%d,\'%s\',\'%s\',\'%s\',%d,%d)',
      $nid,
      $vid,
      1,
      $row['title'],
      _xoops_import_format_text($row["text"], $row['nohtml'] == 0, $row['nosmiley'] == 0, TRUE, $row['nobreaks'] == 0),
      _xoops_import_format_text($row['text'], $row['nohtml'] == 0, $row['nosmiley'] == 0, TRUE, $row['nobreaks'] == 0),
      $row['last_modified'],
      variable_get('xoops_import_input_format', 4)
    );
    db_query('INSERT INTO {node_comment_statistics} (nid, last_comment_timestamp, last_comment_uid) VALUES (%d, %d, %d)',
      $nid,
      $row['datesub'],
      $row['uid']
    );
    
    $output .= _xoops_import_comments($x_tinyd_module_dir, $x_tinyd_module_id, $row['storyid'], 0, $nid, 0, '');
    
    _comment_update_node_statistics($nid);
  }
  
  _xoops_import_module_set_imported($x_tinyd_module_dir);

  print theme('page', $output);
    
  return;
}

?>
