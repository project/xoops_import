<?php

/*
 Xoops tables: xoops_stories(_files|_votedata) xoops_topics
 Xoops folders: uploads/*
*/

function xoops_import_module_news_info() {
  $info = array(
    '#name' => t('News'),
    '#help' => t('Import Xoops news as Drupal stories'),
  );
  
  return $info;
}

function xoops_import_module_news_configure() {
  $form = array();

  $form['news']['xoops_import_module_news_dirname'] = array(
    '#type' => 'textfield',
    '#title' => t('dirname'),
    '#return_value' => 1,
    '#default_value' => variable_get('xoops_import_module_news_dirname', 'news'),
    '#description' => t('The folder name of the News module.'),
  );
  
  $form['news']['xoops_import_module_news_nodetype'] = array(
    '#type' => 'select',
    '#title' => t('Content Type'),
    '#options' => node_get_types('names'),
    '#default_value' => variable_get('xoops_import_module_news_nodetype', 'story'),
    '#description' => t('The content type to import News items into.'),
  );
  
  return $form;
}

function xoops_import_module_news_confirm() {
  $disable = FALSE;
  $catCnt = 0;
  $articleCnt = 0;
  
  _xoops_import_db_connect();
  
  $xoops_import_table_preffix = variable_get('xoops_import_table_preffix', 'xoops');
  $x_news_module_dir = variable_get('xoops_import_module_news_dirname', 'news');

  db_set_active('xoops');
  
  $newsRes = db_query("select COUNT(*) from {$xoops_import_table_preffix}_topics");
  
  if ($newsRes) {
    $catCnt = db_result($newsRes);
  } else {
    $disable = TRUE;
  }
  
  $newsRes = db_query("select COUNT(*) from {$xoops_import_table_preffix}_stories");
  
  if ($newsRes) {
    $articleCnt = db_result($newsRes);
  } else {
    $disable = TRUE;
  }
  
  db_set_active('default');
  
  $form = array();
  
  if (!_xoops_import_module_was_imported('users')) {
    $disable = TRUE;
    
    $form['users'] = array(
      '#value' => '<p>' . t("Import the users first!") . '</p>',
    );
  }
  
  if (_xoops_import_module_was_imported($x_news_module_dir)) {
    $disable = TRUE;
    
    $form['news'] = array(
      '#value' => '<p>' . t("News already imported!") . '</p>',
    );
  }
  
  if (variable_get('xoops_import_module_news_nodetype', '') == '') {
    $disable = TRUE;
    
    $form['nodetype'] = array(
      '#value' => '<p>' . t("Select a content type to import into!") . '</p>',
    );
  }
  
  $form['cat_cnt'] = array(
    '#value' => '<p>' . t("News categories (aka topics): %cats", array('%cats' => $catCnt)) . '</p>',
  );
  
  $form['article_cnt'] = array(
    '#value' => '<p>' . t("News items: %articles", array('%articles' => $articleCnt)) . '</p>',
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
    '#disabled' => $disable,
  );

  return $form;
}

function xoops_import_module_news_import() {
  _xoops_import_db_connect();
  
  $xoops_import_table_preffix = variable_get('xoops_import_table_preffix', 'xoops');
  
  $x_news_module_dir = variable_get('xoops_import_module_news_dirname', 'news');
  $x_news_module_id  = _xoops_import_get_xoops_module_id($x_news_module_dir);
  
  $output = '';
  
  $topic_map = _xoops_import_category(
    $output,
    '',
    'Topic',
    'News item topic created by Xoops Import',
    variable_get('xoops_import_module_news_nodetype', ''),
    'topics',
    'topic_id',
    'topic_title',
    'topic_description',
    'topic_pid',
    $x_news_module_dir
  );
  
  db_set_active('xoops');

  $xoopsContent = db_query("select * from {$xoops_import_table_preffix}_stories ORDER BY storyid");
  
  db_set_active('default');
  
  $fields = node_invoke_nodeapi($node, 'fields');
  
  while ($row = db_fetch_array($xoopsContent)) {
    
    $nid = db_next_id('{node}_nid');
    $vid = db_next_id('{node_revisions}_vid');
    
    _xoops_import_insert_redirect($row['storyid'], $nid, $x_news_module_dir);
    
    $output .= "Importing story {$row['title']}<br/>";
    
    db_query('INSERT INTO {node} (nid,vid,type,title,uid,created,changed,comment,promote) VALUES(%d,%d,\'%s\',\'%s\',%d,%d,%d,%d,%d)',
      $nid,
      $vid,
      variable_get('xoops_import_module_news_nodetype', ''),
      $row['title'],
      $row['uid'],
      $row['created'],
      $row['published'],
      2,
      $row['ihome'] == 0 ? 1 : 0
    );
    db_query('INSERT INTO {node_revisions} (nid,vid,uid,title,body,teaser,timestamp,format) VALUES(%d,%d,%d,\'%s\',\'%s\',\'%s\',%d,%d)',
      $nid,
      $vid,
      $row['uid'],
      $row['title'],
      _xoops_import_format_text($row['hometext'] . "\n\n" . $row["bodytext"], $row['nohtml'] == 0, $row['nosmiley'] == 0),
      _xoops_import_format_text($row['hometext'], $row['nohtml'] == 0, $row['nosmiley'] == 0),
      $row['published'],
      variable_get('xoops_import_input_format', 4)
    );
    db_query('INSERT INTO {term_node} (nid,tid) VALUES(%d,%d)',
      $nid,
      $topic_map[$row['topicid']]
    );
    db_query('INSERT INTO {node_counter} (nid,totalcount) VALUES (%d,%d)',
      $nid,
      $row['counter']
    );
    db_query('INSERT INTO {node_comment_statistics} (nid, last_comment_timestamp, last_comment_uid) VALUES (%d, %d, %d)',
      $nid,
      $row['published'],
      $row['uid']
    );
    
    $output .= _xoops_import_comments($x_news_module_dir, $x_news_module_id, $row['storyid'], 0, $nid, 0, '');
    
    _comment_update_node_statistics($nid);
  }
  
  _xoops_import_module_set_imported($x_news_module_dir);

  print theme('page', $output);
    
  return;
}

?>
