<?php

function xoops_import_module_newbb_info() {
  $info = array(
    '#name' => t('Forums'),
    '#help' => t('Import Xoops newbb forums as Drupal '),
  );
  
  return $info;
}

function xoops_import_module_newbb_configure() {
  $form = array();

  $form['newbb']['xoops_import_module_newbb_dirname'] = array(
    '#type' => 'textfield',
    '#title' => t('dirname'),
    '#return_value' => 1,
    '#default_value' => variable_get('xoops_import_module_newbb_dirname', 'newbb'),
    '#description' => t('The folder name of the Forum module (aka newbb).'),
  );
  
  return $form;
}

function xoops_import_module_newbb_confirm() {
  $disable = FALSE;
  $catCnt = 0;
  $forumCnt = 0;
  $postCnt = 0;
  
  _xoops_import_db_connect();
  
  $xoops_import_table_preffix = variable_get('xoops_import_table_preffix', 'xoops');
  $xoops_newbb_module_dir = variable_get('xoops_import_module_newbb_dirname', 'newbb');

  db_set_active('xoops');
  
  $newbbRes = db_query("select COUNT(*) from {$xoops_import_table_preffix}_bb_categories");
  
  if ($newbbRes) {
    $catCnt = db_result($newbbRes);
  } else {
    $disable = TRUE;
  }
  
  $newbbRes = db_query("select COUNT(*) from {$xoops_import_table_preffix}_bb_forums");
  
  if ($newbbRes) {
    $forumCnt = db_result($newbbRes);
  } else {
    $disable = TRUE;
  }
  
  $newbbRes = db_query("select COUNT(*) from {$xoops_import_table_preffix}_bb_posts");
  
  if ($newbbRes) {
    $postCnt = db_result($newbbRes);
  } else {
    $disable = TRUE;
  }
  
  db_set_active('default');
  
  $form = array();
  
  if (!_xoops_import_module_was_imported('users')) {
    $disable = TRUE;
    
    $form['users'] = array(
      '#value' => '<p>' . t("Import the users first!") . '</p>',
    );
  }
  
  if (_xoops_import_module_was_imported($xoops_newbb_module_dir)) {
    $disable = TRUE;
    
    $form['newbb'] = array(
      '#value' => '<p>' . t("Forums already imported!") . '</p>',
    );
  }
  
  $form['cat_cnt'] = array(
    '#value' => '<p>' . t("Forum categories: %cats", array('%cats' => $catCnt)) . '</p>',
  );
  
  $form['forum_cnt'] = array(
    '#value' => '<p>' . t("Forums: %forums", array('%forums' => $forumCnt)) . '</p>',
  );
  
  $form['post_cnt'] = array(
    '#value' => '<p>' . t("Forum posts: %posts", array('%posts' => $postCnt)) . '</p>',
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
    '#disabled' => $disable,
  );
  
  return $form;
}

function xoops_import_module_newbb_import() {
  _xoops_import_db_connect();
  
  $xoops_import_table_preffix = variable_get('xoops_import_table_preffix', 'xoops');
  $xoops_newbb_module_dir = variable_get('xoops_import_module_newbb_dirname', 'newbb');
  
  db_set_active('xoops');

  $xoopsForums = db_query("select * from {$xoops_import_table_preffix}_bb_categories ORDER BY cat_id");
  
  db_set_active('default');
  
  $vid = _forum_get_vid();
  
  $cid_translation = array();
  
  while ($row = db_fetch_array($xoopsForums)) {
    $edit = array();
    
    $edit['name'] = $row['cat_title'];
    $edit['description'] = $row['cat_description'];
    $edit['vid'] = $vid;
    $edit['weight'] = $row['cat_order'];
    
    taxonomy_save_term($edit);
    
    $cid_translation[$row['cat_id']] = $edit['tid'];
    
    _xoops_import_insert_redirect($row['cat_id'], $edit['tid'], $xoops_newbb_module_dir, 'category');
  }

  db_set_active('xoops');

  $xoopsForums = db_query("select * from {$xoops_import_table_preffix}_bb_forums ORDER BY forum_id");
  
  db_set_active('default');
  
  $lastId = 0;

  $tid_translation = array();
  
  while ($row = db_fetch_array($xoopsForums)) {
    $edit = array();
    
    // check if forum is disabled
    if ($row['forum_type'] == 1)
      continue;
    
    if ($row['parent_forum'] == 0) {
      $edit["parent"][0] = $cid_translation[$row['cat_id']];
    } else {
      $edit["parent"][0] = $tid_translation[$row['parent_forum']];	
    }
    
    $edit['name'] = $row['forum_name'];
    $edit['description'] = $row['forum_desc'];
    $edit['vid'] = $vid;
    $edit['weight'] = 0;
    
    taxonomy_save_term($edit);
    
    $tid_translation[$row['forum_id']] = $edit['tid'];	
  }
  
  db_set_active('xoops');
  
  $xoopsPosts = db_query("SELECT * FROM {$xoops_import_table_preffix}_bb_posts, {$xoops_import_table_preffix}_bb_posts_text WHERE {$xoops_import_table_preffix}_bb_posts.post_id = {$xoops_import_table_preffix}_bb_posts_text.post_id AND {$xoops_import_table_preffix}_bb_posts.pid = %d AND {$xoops_import_table_preffix}_bb_posts_text.post_edit = '' ORDER BY {$xoops_import_table_preffix}_bb_posts.post_time", 0);
  
  db_set_active('default');
  
  $fields = node_invoke_nodeapi($node, 'fields');
  
  while ($row = db_fetch_array($xoopsPosts)) {

    $nid = db_next_id('{node}_nid');
    $vid = db_next_id('{node_revisions}_vid');
    
    _xoops_import_insert_redirect($row["topic_id"], $nid, $xoops_newbb_module_dir);
    
    $output .= "Importing post: {$row['subject']}<br/>";
    
    db_query('INSERT INTO {node} (nid,vid,type,title,uid,created,changed,comment) VALUES(%d,%d,\'%s\',\'%s\',%d,%d,%d,%d)',
      $nid,
      $vid,
      'forum',
      $row['subject'],
      $row['uid'],
      $row['post_time'],
      $row['post_time'],
      2
    );
    db_query('INSERT INTO {node_revisions} (nid,vid,uid,title,body,teaser,timestamp,format) VALUES(%d,%d,%d,\'%s\',\'%s\',\'%s\',%d,%d)',
      $nid,
      $vid,
      $row['uid'],
      $row['subject'],
      _xoops_import_format_text($row['post_text'], $row['dohtml'] == 1, $row['dosmiley'] == 1, $row['doxcode'] == 1, $row['dobr'] == 1),
      _xoops_import_format_text(node_teaser($row['post_text'], variable_get('xoops_import_input_format', 4)), $row['dohtml'] == 1, $row['dosmiley'] == 1, $row['doxcode'] == 1, $row['dobr'] == 1),
      $row['post_time'],
      variable_get('xoops_import_input_format', 4)
    );
    db_query('INSERT INTO {term_node} (nid,tid) VALUES(%d,%d)',
      $nid,
      $tid_translation[$row['forum_id']]
    );
    db_query('INSERT INTO {forum} (nid,vid,tid) VALUES (%d,%d,%d)',
      $nid,
      $vid,
      $tid_translation[$row['forum_id']]
    );
    db_query('INSERT INTO {node_comment_statistics} (nid, last_comment_timestamp, last_comment_uid) VALUES (%d, %d, %d)',
      $nid,
      $row['post_time'],
      $row['uid']
    );
    
    $output .= _xoops_import_newbb_insert_comments($xoops_newbb_module_dir, $row['post_id'], $nid, 0, '');
    
    _comment_update_node_statistics($nid);
  }
  
  _xoops_import_module_set_imported($xoops_newbb_module_dir);

  print theme('page', $output);
  
  return;
}

function _xoops_import_newbb_insert_comments($x_module_dir, $x_post_id, $nid, $parent_id, $thread) {
  $xoops_import_table_preffix = variable_get("xoops_import_table_preffix", "xoops");

  db_set_active('xoops');
  
  $xoopsPosts = db_query("SELECT * FROM {$xoops_import_table_preffix}_bb_posts, {$xoops_import_table_preffix}_bb_posts_text WHERE {$xoops_import_table_preffix}_bb_posts.post_id = {$xoops_import_table_preffix}_bb_posts_text.post_id AND {$xoops_import_table_preffix}_bb_posts.pid = %d AND {$xoops_import_table_preffix}_bb_posts_text.post_edit = '' ORDER BY {$xoops_import_table_preffix}_bb_posts.post_time", $x_post_id);
  
  db_set_active('default');

  $fields = node_invoke_nodeapi($node, 'fields');
  
  $output = '';
  
  $thread_i = 0;
  $cur_thread = "";

  while ($row = db_fetch_array($xoopsPosts)) {
    // Strip the "/" from the end of the thread.
    $thread = rtrim($thread, '/');
    
    $thread_i++;
    
    if ($thread_i == 10) {
      $thread_i = 90;
    } elseif ($thread_i == 100) {
      $thread_i = 900;
    }
    
    // Finally, build the thread field for this new comment.
    if ($thread == "") {
      $cur_thread = $thread_i .'/';
    } else {
      $cur_thread = $thread . "." . $thread_i .'/';			
    }
    
    $cid = db_next_id('{comments}_cid');
    
    $status = 0;
    $scode = 0;
    $users = serialize(array(0 => 0));
    $account = user_load(array('uid' => $row["uid"]));
    
    db_query("INSERT INTO {comments} (cid, nid, pid, uid, subject, comment, format, hostname, timestamp, status, score, users, thread, name, mail) VALUES (%d, %d, %d, %d, '%s', '%s', %d, '%s', %d, %d, %d, '%s', '%s', '%s', '%s')", 
      $cid,
      $nid,
      $parent_id,
      $row["uid"],
      $row["subject"], 
      _xoops_import_format_text($row["post_text"], $row['dohtml'] == 1, $row['dosmiley'] == 1, $row['doxcode'] == 1, $row['dobr'] == 1),
      variable_get('xoops_import_input_format', 4),
      _xoops_import_format_ip($row['poster_ip']),
      $row["post_time"], 
      $status,
      $score,
      $users,
      $cur_thread,
      $account->name,
      $account->mail
    );
    
    $output .= "Imported reply: {$row['subject']}<br/>";
    
    _xoops_import_insert_redirect($row["post_id"], $cid, $x_module_dir, 'comment');
    
    $output .= _xoops_import_newbb_insert_comments($x_module_dir, $row["post_id"], $nid, $cid, $cur_thread);
  }
  
  return $output;
}

?>
