<?php

function xoops_import_module_smartsection_info() {
  $info = array(
    '#name' => t('SmartSection'),
    '#help' => t('Import Xoops SmartSection articles as Drupal nodes'),
  );
  
  return $info;
}

function xoops_import_module_smartsection_configure() {
  $form = array();

  $form['smartsection']['xoops_import_module_smartsection_dirname'] = array(
    '#type' => 'textfield',
    '#title' => t('dirname'),
    '#default_value' => variable_get('xoops_import_module_smartsection_dirname', 'smartsection'),
    '#description' => t('The folder name of the SmartSection module.'),
  );

  $form['smartsection']['xoops_import_module_smartsection_basetablename'] = array(
    '#type' => 'textfield',
    '#title' => t('base table name'),
    '#field_prefix' => 'xoops_',
    '#field_suffix' => '_(categories|files|items|meta|mimetyes)',
    '#size' => 10,
    '#default_value' => variable_get('xoops_import_module_smartsection_basetablename', 'smartsection'),
    '#description' => t('The base table name of the SmartSection module, this would be different from the default only if the module was cloned using search and replace and installed several times.'),
  );
  
  $form['smartsection']['xoops_import_module_smartsection_nodetype'] = array(
    '#type' => 'select',
    '#title' => t('Content Type'),
    '#options' => node_get_types('names'),
    '#default_value' => variable_get('xoops_import_module_smartsection_nodetype', 'story'),
    '#description' => t('The content type to import SmartSection articles into.'),
  );

  return $form;
}

function xoops_import_module_smartsection_confirm() {
  $smartsection_base_table_name = variable_get('xoops_import_module_smartsection_basetablename', 'smartsection');
  $x_smartsection_module_dir = variable_get('xoops_import_module_smartsection_dirname', 'smartsection');
  
  $disable = FALSE;
  $catCnt = 0;
  $articleCnt = 0;
  
  _xoops_import_db_connect();
  
  $xoops_import_table_preffix = variable_get('xoops_import_table_preffix', 'xoops');

  db_set_active('xoops');
  
  $smartsectionRes = db_query("select COUNT(*) from {$xoops_import_table_preffix}_{$smartsection_base_table_name}_categories");
  
  if ($smartsectionRes) {
    $catCnt = db_result($smartsectionRes);
  } else {
    $disable = TRUE;
  }
  
  $smartsectionRes = db_query("select COUNT(*) from {$xoops_import_table_preffix}_{$smartsection_base_table_name}_items");
  
  if ($smartsectionRes) {
    $articleCnt = db_result($smartsectionRes);
  } else {
    $disable = TRUE;
  }
  
  db_set_active('default');
  
  $form = array();
  
  if (!_xoops_import_module_was_imported('users')) {
    $disable = TRUE;
    
    $form['users'] = array(
      '#value' => '<p>' . t("Import the users first!") . '</p>',
    );
  }
  
  if (_xoops_import_module_was_imported($x_smartsection_module_dir)) {
    $disable = TRUE;
    
    $form['smartsection'] = array(
      '#value' => '<p>' . t("SmartSection already imported!") . '</p>',
    );
  }
  
  if (variable_get('xoops_import_module_smartsection_nodetype', '') == '') {
    $disable = TRUE;
    
    $form['nodetype'] = array(
      '#value' => '<p>' . t("Select a content type to import into!") . '</p>',
    );
  }
  
  $form['cat_cnt'] = array(
    '#value' => '<p>' . t("SmartSection categories: %cats", array('%cats' => $catCnt)) . '</p>',
  );
  
  $form['article_cnt'] = array(
    '#value' => '<p>' . t("SmartSection: %articles", array('%articles' => $articleCnt)) . '</p>',
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
    '#disabled' => $disable,
  );
  
  return $form;
}

function xoops_import_module_smartsection_import() {
  $smartsection_base_table_name = variable_get('xoops_import_module_smartsection_basetablename', 'smartsection');
  
  $x_smartsection_module_dir = variable_get('xoops_import_module_smartsection_dirname', 'smartsection');
  $x_smartsection_module_id  = _xoops_import_get_xoops_module_id($x_smartsection_module_dir);
  
  _xoops_import_db_connect();
  
  $xoops_import_table_preffix = variable_get('xoops_import_table_preffix', 'xoops');
  
  $output = '';
  
  $cat_map = _xoops_import_category(
    $output,
    '',
    'SmartSection Category',
    'SmartSection article category',
    variable_get('xoops_import_module_smartsection_nodetype', ''),
    $smartsection_base_table_name . '_categories',
    'categoryid',
    'name',
    'description',
    'parentid',
    $x_smartsection_module_dir
  );
  
  db_set_active('xoops');

  $xoopsArticles = db_query("select * from {$xoops_import_table_preffix}_{$smartsection_base_table_name}_items ORDER BY itemid");
  
  db_set_active('default');
  
  $fields = node_invoke_nodeapi($node, 'fields');
  
  while ($row = db_fetch_array($xoopsArticles)) {
    
    $nid = db_next_id('{node}_nid');
    $vid = db_next_id('{node_revisions}_vid');
    
    _xoops_import_insert_redirect($row['itemid'], $nid, $x_smartsection_module_dir);
    
    $output .= "Importing article {$row['title']}<br/>";
  
    db_query('INSERT INTO {node} (nid,vid,type,title,uid,created,changed,comment,promote) VALUES(%d,%d,\'%s\',\'%s\',%d,%d,%d,%d,%d)',
      $nid,
      $vid,
      variable_get('xoops_import_module_smartsection_nodetype', ''),
      $row['title'],
      $row['uid'],
      $row['datesub'],
      $row['datesub'],
      $row['cancomment'] == 1 ? 2 : 1,
      0
    );
    db_query('INSERT INTO {node_revisions} (nid,vid,uid,title,body,teaser,timestamp,format) VALUES(%d,%d,%d,\'%s\',\'%s\',\'%s\',%d,%d)',
      $nid,
      $vid,
      $row['uid'],
      $row['title'],
      _xoops_import_format_text($row['summary'] . "\n\n" . $row["body"], $row['dohtml'] == 1, $row['dosmiley'] == 1, $row['doxcode'] == 1, $row['dobr'] == 1),
      _xoops_import_format_text($row['summary'], $row['dohtml'] == 1, $row['dosmiley'] == 1, $row['doxcode'] == 1, $row['dobr'] == 1),
      $row['datesub'],
      variable_get('xoops_import_input_format', 4)
    );
    db_query('INSERT INTO {term_node} (nid,tid) VALUES(%d,%d)',
      $nid,
      $cat_map[$row['categoryid']]
    );
    db_query('INSERT INTO {node_counter} (nid,totalcount) VALUES (%d,%d)',
      $nid,
      $row['counter']
    );
    db_query('INSERT INTO {node_comment_statistics} (nid, last_comment_timestamp, last_comment_uid) VALUES (%d, %d, %d)',
      $nid,
      $row['datesub'],
      $row['uid']
    );
    
    $output .= _xoops_import_comments($x_smartsection_module_dir, $x_smartsection_module_id, $row['itemid'], 0, $nid, 0, '');
    
    _comment_update_node_statistics($nid);
  }
  
  _xoops_import_module_set_imported($x_smartsection_module_dir);

  print theme('page', $output);
    
  return;
}

?>
