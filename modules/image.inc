<?php

/*
 Xoops tables: xoops_image, xoops_imagebody, xoops_imagecategory
 Xoops folders: uploads/*
 
*/

function xoops_import_module_image_info() {
  $info = array(
    '#name' => t('System Images'),
    '#help' => t('Import Xoops system images as Drupal images'),
  );
  
  return $info;
}

function xoops_import_module_image_configure() {
  $form = array();
  
  return $form;
}

function xoops_import_module_image_confirm() {
  $disable = FALSE;
  $catCnt = 0;
  $imageCnt = 0;
  
  _xoops_import_db_connect();
  
  $xoops_import_table_preffix = variable_get('xoops_import_table_preffix', 'xoops');

  db_set_active('xoops');
  
  $imageRes = db_query("select COUNT(*) from {$xoops_import_table_preffix}_imagecategory");
  
  if ($imageRes) {
    $catCnt = db_result($imageRes);
  } else {
    $disable = TRUE;
  }
  
  $imageRes = db_query("select COUNT(*) from {$xoops_import_table_preffix}_image");
  
  if ($imageRes) {
    $imageCnt = db_result($imageRes);
  } else {
    $disable = TRUE;
  }
  
  db_set_active('default');
  
  $form = array();
  
  if (_xoops_import_module_was_imported('image')) {
    $disable = TRUE;
    
    $form['image'] = array(
      '#value' => '<p>' . t("Images already imported!") . '</p>',
    );
  }
  
  if (variable_get('xoops_import_base_folder', '') == '') {
    $disable = TRUE;
    
    $form['folder'] = array(
      '#value' => '<p>' . t("Xoops base folder not set!") . '</p>',
    );
  }
  
  $form['cat_cnt'] = array(
    '#value' => '<p>' . t("Image categories: %cats", array('%cats' => $catCnt)) . '</p>',
  );
  
  $form['image_cnt'] = array(
    '#value' => '<p>' . t("Images: %images", array('%images' => $imageCnt)) . '</p>',
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
    '#disabled' => $disable,
  );
  
  return $form;
}

function xoops_import_module_image_import() {
  _xoops_import_db_connect();
  
  $xoops_import_table_preffix = variable_get('xoops_import_table_preffix', 'xoops');
  
  $output = '';
  
  $cat_map = _xoops_import_category(
    $output,
    '',
    'Image Category',
    'Image Categories created by Xoops Import based on Xoops system images',
    'image',
    'imagecategory',
    'imgcat_id',
    'imgcat_name',
    '',
    '',
    'image'
  );
  
  db_set_active('xoops');

  $xoopsImages = db_query("select * from {$xoops_import_table_preffix}_image ORDER BY image_weight, image_id");
  
  db_set_active('default');

  $base_folder = variable_get('xoops_import_base_folder', '') . '/uploads/';
  
  while ($row = db_fetch_array($xoopsImages)) {
    $filepath = $base_folder . $row['image_name'];
    
    $output .= "Importing system image {$row['image_nicename']}<br/>";
    
    $node = _xoops_import_create_image(
      $filepath,
      $row['image_nicename'],
      '',
      $cat_map[$row['imgcat_id']],
      NULL,
      $row['image_created']
    );
    
    if ($node) {
      $nid = $node->nid;
      
      _xoops_import_insert_redirect($row['image_id'], $nid, 'image');
    } else {
      drupal_set_message('Importing system image ' . $filepath . ' failed.', 'error');
    }
  }
  
  _xoops_import_module_set_imported('image');
  
  print theme('page', $output);
}

?>