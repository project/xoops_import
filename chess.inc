<?php

function algebraic2FEN($algebraic) {
  list($whites,$blacks) = explode(";", $algebraic);
  $white = explode(",", $whites);
  $black = explode(",", $blacks);
  
  $board = array();
  
  foreach($white as $piece) {
    $piece = trim($piece);
    
    if (strlen($piece) == 3) {
      $symbol = strtoupper(substr($piece,0,1));
      $file = strtolower(substr($piece,1,1));
      $rank = intval(substr($piece,2,1));
    } elseif (strlen($piece) == 2) {
      $symbol = 'P';
      $file = strtolower(substr($piece,0,1));
      $rank = intval(substr($piece,1,1));
    }
    
    $board[$file][$rank] = $symbol;
  }
  
  foreach($black as $piece) {
    $piece = trim($piece);
    
    if (strlen($piece) == 3) {
      $symbol = strtolower(substr($piece,0,1));
      $file = strtolower(substr($piece,1,1));
      $rank = intval(substr($piece,2,1));
    } elseif (strlen($piece) == 2) {
      $symbol = 'p';
      $file = strtolower(substr($piece,0,1));
      $rank = intval(substr($piece,1,1));
    }
    
    $board[$file][$rank] = $symbol;
  }
  
  $fen = '';
  
  foreach(array(8,7,6,5,4,3,2,1) as $rank) {
    $empties = 0;
    foreach(array('a','b','c','d','e','f','g','h') as $file) {
      if (isset($board[$file][$rank])) {
        if ($empties > 0) {
          $fen .= $empties;
          $empties = 0;
        }
        $fen .= $board[$file][$rank];
      } else {
        $empties++;
      }
    }
    
    if ($empties > 0) {
      $fen .= $empties;
    }
    
    if ($rank > 1) {
      $fen .= '/';
    }
  }
  
  return $fen;
}

//print algebraic2FEN('Ke1,Qd2,Ra1,Rh1,Nf1,Be3,a2,b2,c3,f2,g2,h2;Kg8,Qf7,Re8,Rf8,Bd6,Bg4,a6,b7,c7,d5,g7,h7') . '<br>';
//print preg_replace_callback("/\[chess](.*)\[\/chess\]/sU", create_function('$matches', 'return "[chessboard]" . algebraic2FEN($matches[1]) . "[/chessboard]";'), '[chess]Ke1,Qd2,Ra1,Rh1,Nf1,Be3,a2,b2,c3,f2,g2,h2;Kg8,Qf7,Re8,Rf8,Bd6,Bg4,a6,b7,c7,d5,g7,h7[/chess] and [chess];[/chess]');
?>