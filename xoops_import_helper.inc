<?php

function _xoops_import_get_xoops_module_id($xoops_module_dirname) {
  _xoops_import_db_connect();
  
  $xoops_import_table_preffix = variable_get('xoops_import_table_preffix', 'xoops');
  
  db_set_active('xoops');

  $xoopsMid = db_query("select mid from {$xoops_import_table_preffix}_modules WHERE dirname='$xoops_module_dirname'");
  
  db_set_active('default');
  
  return db_result($xoopsMid);
}

function _xoops_import_insert_redirect($xoops_id, $drupal_id, $xoops_module, $data = 'item') {
  db_query('INSERT INTO {xoops_redirect} (xoops_id, drupal_id, xoops_module, data) VALUES (%d, %d, "%s", "%s")',
    $xoops_id,
    $drupal_id,
    $xoops_module,
    $data);	
}

function _xoops_import_format_text($text, $doHtml = FALSE, $doSmiley = TRUE, $doXcode = TRUE, $doBr = TRUE) {
  if (!$doHtml) {
    $text = htmlspecialchars($text, ENT_QUOTES);
  }
  
  if (!$doSmiley) {
    // html encode colon and right parantheses, : and ), this should break all standard Xoops smilies
    $text = preg_replace(array("/\:/", "/\(/"), array("&#58;", "&#41;"), $text);
  }
  
  if (!$doXcode) {
    // html encode left square bracket, [, this should break all Xoops codes
    $text = preg_replace("/\[/", "&#91;", $text);
  }
  
  if ($doBr) {
    $text = preg_replace("/(\015\012)|(\015)|(\012)/","<br />",$text);
  }
  
  return _xoops_import_xcode_decode($text);
}

function _xoops_import_xcode_decode($text) {
  global $base_url;
  
//  include_once 'chess.inc';
//  $text = preg_replace_callback("/\[chess](.*)\[\/chess\]/sU", create_function('$m', 'return "[chessboard]" . algebraic2FEN($m[1]) . "[/chessboard]";'), $text);
  
  $site_url = '';
  
  if (isset($base_url)) {
    $site_url = $base_url;
  }
  
  $patterns = array();
  $replacements = array();
  $patterns[] = "/\[siteurl=(['\"]?)([^\"'<>]*)\\1](.*)\[\/siteurl\]/sU";
  $replacements[] = '<a href="'.$site_url.'/\\2" target="_blank">\\3</a>';
  $patterns[] = "/\[url=(['\"]?)(http[s]?:\/\/[^\"'<>]*)\\1](.*)\[\/url\]/sU";
  $replacements[] = '<a href="\\2" target="_blank">\\3</a>';
  $patterns[] = "/\[url=(['\"]?)(ftp?:\/\/[^\"'<>]*)\\1](.*)\[\/url\]/sU";
  $replacements[] = '<a href="\\2" target="_blank">\\3</a>';
  $patterns[] = "/\[url=(['\"]?)([^\"'<>]*)\\1](.*)\[\/url\]/sU";
  $replacements[] = '<a href="http://\\2" target="_blank">\\3</a>';
  $patterns[] = "/\[color=(['\"]?)([a-zA-Z0-9]*)\\1](.*)\[\/color\]/sU";
  $replacements[] = '<span style="color: #\\2;">\\3</span>';
  $patterns[] = "/\[size=(['\"]?)([a-z0-9-]*)\\1](.*)\[\/size\]/sU";
  $replacements[] = '<span style="font-size: \\2;">\\3</span>';
  $patterns[] = "/\[font=(['\"]?)([^;<>\*\(\)\"']*)\\1](.*)\[\/font\]/sU";
  $replacements[] = '<span style="font-family: \\2;">\\3</span>';
  $patterns[] = "/\[email]([^;<>\*\(\)\"']*)\[\/email\]/sU";
  $replacements[] = '<a href="mailto:\\1">\\1</a>';
  $patterns[] = "/\[b](.*)\[\/b\]/sU";
  $replacements[] = '<b>\\1</b>';
  $patterns[] = "/\[i](.*)\[\/i\]/sU";
  $replacements[] = '<i>\\1</i>';
  $patterns[] = "/\[u](.*)\[\/u\]/sU";
  $replacements[] = '<u>\\1</u>';
  $patterns[] = "/\[d](.*)\[\/d\]/sU";
  $replacements[] = '<del>\\1</del>';
  $patterns[] = "/\[img align=(['\"]?)(left|center|right)\\1]([^\"\(\)\?\&'<>]*)\[\/img\]/sU";
  $patterns[] = "/\[img]([^\"\(\)\?\&'<>]*)\[\/img\]/sU";
  $patterns[] = "/\[img align=(['\"]?)(left|center|right)\\1 id=(['\"]?)([0-9]*)\\3]([^\"\(\)\?\&'<>]*)\[\/img\]/sU";
  $patterns[] = "/\[img id=(['\"]?)([0-9]*)\\1]([^\"\(\)\?\&'<>]*)\[\/img\]/sU";
  $replacements[] = '<img src="\\3" align="\\2" alt="" />';
  $replacements[] = '<img src="\\1" alt="" />';
  $replacements[] = '<img src="'.$site_url.'/image.php?id=\\4" align="\\2" alt="\\4" />';
  $replacements[] = '<img src="'.$site_url.'/image.php?id=\\2" alt="\\3" />';
  $patterns[] = "/\[quote]/sU";
  $replacements[] = t('Quote: ').'<blockquote class="xoopsQuote">';
  $patterns[] = "/\[\/quote]/sU";
  $replacements[] = '</blockquote>';
  $text = str_replace( "\x00", "", $text );
  $c = "[\x01-\x1f]*";
  $patterns[] = "/j{$c}a{$c}v{$c}a{$c}s{$c}c{$c}r{$c}i{$c}p{$c}t{$c}:/si";
  $replacements[] = "(tammairanslip)";
  $patterns[] = "/a{$c}b{$c}o{$c}u{$c}t{$c}:/si";
  $replacements[] = "about :";
  
  return preg_replace($patterns, $replacements, $text);
}

function _xoops_import_category(&$output, $vid, $name, $description, $node_type, $table, $col_id, $col_title, $col_description, $col_parent, $xoops_module_dir) {
  _xoops_import_db_connect();
  
  $xoops_import_table_preffix = variable_get('xoops_import_table_preffix', 'xoops');
  
  if (empty($vid)) {
    $output .= 'Creating category: ' . $description . '<br />';
    
    $edit['name'] = $name;
    $edit['description'] = $description;
    $edit['help'] = 'Select a category';
    $edit['multiple'] = 0;
    $edit['required'] = 0;
    $edit['hierarchy'] = empty($col_parent) ? 0 : 1;
    $edit['relations'] = 0;
    $edit['tags'] = 0;
    $edit['weight'] = 0;
    $edit['module'] = 'taxonomy';
    $edit['nodes'] = array($node_type => TRUE);
    
    taxonomy_save_vocabulary($edit);
    
    $vid = $edit['vid'];
  }
  
  db_set_active('xoops');

  $xoopsTopics = db_query("select * from {$xoops_import_table_preffix}_$table ORDER BY $col_id");
  
  db_set_active('default');
  
  $cat_map = array();
  
  while ($row = db_fetch_array($xoopsTopics)) {
    $output .= "Importing category: {$row[$col_title]}<br />";
    
    $edit = array();
    
    $edit['name'] = $row[$col_title];
    if (!empty($col_description)) {
      $edit['description'] = $row[$col_description];
    }
    $edit['vid'] = $vid;
    $edit['weight'] = 0;
    if (!empty($col_parent)) {
      $edit['parent'] = $cat_map[$row[$col_parent]];
    }
    
    taxonomy_save_term($edit);
    
    $cat_map[$row[$col_id]] = $edit['tid'];
    
    _xoops_import_insert_redirect($row[$col_id], $edit['tid'], $xoops_module_dir, 'category');
  }
  
  return $cat_map;
}

function _xoops_import_format_ip($ip) {
  return '' . $ip; // TODO convert number to IP
}

function _xoops_import_comments($x_module_dir, $x_module_id, $x_item_id, $x_parent_id, $nid, $parent_id, $thread) {
  $xoops_import_table_preffix = variable_get("xoops_import_table_preffix", "xoops");
  
  db_set_active('xoops');
  
  $xoopsComments = db_query("SELECT * FROM {$xoops_import_table_preffix}_xoopscomments WHERE com_modid = %d AND com_itemid = %d AND com_pid = %d AND com_status = 2 ORDER BY com_created",
    $x_module_id,
    $x_item_id,
    $x_parent_id
  );
  
  db_set_active('default');

  $fields = node_invoke_nodeapi($node, 'fields');
  
  $output = '';
  
  $thread_i = 0;
  $cur_thread = "";

  while ($row = db_fetch_array($xoopsComments)) {
    // Strip the "/" from the end of the thread.
    $thread = rtrim($thread, '/');
    
    $thread_i++;
    
    if ($thread_i == 10) {
      $thread_i = 90;
    } elseif ($thread_i == 100) {
      $thread_i = 900;
    }
    
    // Finally, build the thread field for this new comment.
    if ($thread == "") {
      $cur_thread = $thread_i .'/';
    } else {
      $cur_thread = $thread . "." . $thread_i .'/';			
    }
    
    $cid = db_next_id('{comments}_cid');
    
    $status = 0;
    $scode = 0;
    $users = serialize(array(0 => 0));
    $account = user_load(array('uid' => $row["com_uid"]));
    
    db_query("INSERT INTO {comments} (cid, nid, pid, uid, subject, comment, format, hostname, timestamp, status, score, users, thread, name, mail) VALUES (%d, %d, %d, %d, '%s', '%s', %d, '%s', %d, %d, %d, '%s', '%s', '%s', '%s')", 
      $cid,
      $nid,
      $parent_id,
      $row["com_uid"],
      $row["com_title"], 
      _xoops_import_format_text($row["com_text"], $row['dohtml'] == 1, $row['dosmiley'] == 1, $row['doxcode'] == 1, $row['dobr'] == 1),
      variable_get('xoops_import_input_format', 4),
      _xoops_import_format_ip($row['com_ip']),
      $row["com_created"], 
      $status,
      $score,
      $users,
      $cur_thread,
      $account->name,
      $account->mail
    );
    
    $output .= "Imported comment: {$row['com_title']}<br/>";
    
    _xoops_import_insert_redirect($row['com_id'], $cid, $x_module_dir, 'comment');
    
    $output .= _xoops_import_comments($x_module_dir, $x_module_id, $x_item_id, $row['com_id'], $nid, $cid, $cur_thread);
  }
  
  return $output;
}

/**
 * Copied from image.module
 **/
function _xoops_import_create_image($filepath, $title = NULL, $body = '', $taxonomy = NULL, $uid = NULL, $date = NULL, $hits = NULL) {
  // Ensure it's a valid image.
  if (!$image_info = image_get_info($filepath)) {
    return FALSE;
  }
  
  // Make sure we can copy the file into our temp directory.
  $original_path = $filepath;
  if (!file_copy($filepath, _image_filename($filepath, IMAGE_ORIGINAL, TRUE))) {
    return FALSE;
  }

  // Resize the original image.
  $aspect_ratio = $image_info['height'] / $image_info['width'];
  $size = image_get_sizes(IMAGE_ORIGINAL, $aspect_ratio);
  if (!empty($size['width']) && !empty($size['height'])) {
    image_scale($filepath, $filepath, $size['width'], $size['height']);
  }
  
  if (!$uid) {
    $uid = 1;
  }
  
  // Build the node.
  $node = new stdClass();
  $node->type = 'image';
  $node->uid = $uid;
//  $node->name = $user->name;
  $node->title = isset($title) ? $title : basename($filepath);
  $node->body = $body;

  // Set the node's defaults... (copied this from node and comment.module)
  $node_options = variable_get('node_options_'. $node->type, array('status', 'promote'));
  $node->status = in_array('status', $node_options);
  $node->promote = in_array('promote', $node_options);
  if (module_exists('comment')) {
    $node->comment = variable_get("comment_$node->type", COMMENT_NODE_READ_WRITE);
  }

  $node->new_file = TRUE;
  $node->images[IMAGE_ORIGINAL] = $filepath;
  _image_build_derivatives($node, TRUE);

  // Save the node.
  $node = node_submit($node);
  node_save($node);
  
  if ($taxonomy) {
    db_query('INSERT INTO {term_node} (nid,tid) VALUES(%d,%d)',
      $node->nid,
      $taxonomy
    );
  }
  
  if ($date) {
    db_query('UPDATE {node} SET uid=%d, created=%d, changed=%d WHERE nid=%d',
      $uid,
      $date,
      $date,
      $node->nid);
  } else {
    db_query('UPDATE {node} SET uid=%d WHERE nid=%d',
      $uid,
      $node->nid);
  }
  
  db_query('UPDATE {node_revisions} SET uid=%d WHERE nid=%d',
    $uid,
    $node->nid);
  
  if ($hits) {
    db_query('INSERT INTO {node_counter} (nid,totalcount) VALUES (%d,%d)',
      $node->nid,
      $hits
    );
  }
  
  return $node;
}

function _xoops_import_module_was_imported($xoops_module_dir) {
  return in_array($xoops_module_dir, variable_get('xoops_import_imported', array()));
}

function _xoops_import_module_set_imported($xoops_module_dir) {
  $mods = variable_get('xoops_import_imported', array());
  
  $mods[] = $xoops_module_dir;
  
  variable_set('xoops_import_imported', $mods);
}

?>
